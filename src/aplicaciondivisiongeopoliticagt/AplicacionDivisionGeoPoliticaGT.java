
package aplicaciondivisiongeopoliticagt;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Josue Daniel Roldan Ochoa.
 * 
 * 
 * 
 */

public class AplicacionDivisionGeoPoliticaGT {

public AplicacionDivisionGeoPoliticaGT(){
    
}
    
public static void main(String[] args) throws IOException {
DivisionPoliticaGT();
}

private static void DivisionPoliticaGT() throws IOException{
String[]dep= new String[22];
String [] cab= new String[22];

dep[0]= "Alta Verapaz";
dep[1]="Baja Verapaz";
dep[2]="Chimaltenango";
dep[3]="Chiquimula";
dep[4]="Peten";
dep[5]="El Progreso";
dep[6]="Quiche";
dep[7]="Escuintla";
dep[8]="Guatemala";
dep[9]="Huehuetenango";
dep[10]="Izabal";
dep[11]="Jalapa";
dep[12]="Jutiapa";
dep[13]="Quetzaltenango";
dep[14]="Retalhuleu";
dep[15]="Sacatepequez";
dep[16]="San Marcos";
dep[17]="Santa Rosa";
dep[18]="Solola";
dep[19]="Suchitepequez";
dep[20]="Totonicapan";
dep[21]="Zacapa";
   
cab[0]="Coban";
cab[1]="Salama";
cab[2]="Chimaltenango";
cab[3]="Chiquimula";
cab[4]="Flores";
cab[5]="Guastatoya";
cab[6]="Santa Cruz del Quiche";
cab[7]="Escuintla";
cab[8]="Guatemala";
cab[9]="Huehuetenango";
cab[10]="Puerto Barrios";
cab[11]="Jalapa";
cab[12]="Jutiapa";
cab[13]="Quetzaltenango";
cab[14]="Retalhuleu";
cab[15]="Antigua Guatemala";
cab[16]="San Marcos";
cab[17]="Cuilapa";
cab[18]="Solola";
cab[19]="Mazatenango";
cab[20]="Totonicapan";
cab[21]="Zacapa";
    
    
String[]resp=new String[22];
int[]resp1=new int[22];
BufferedReader bf= new BufferedReader(new InputStreamReader(System.in));
System.out.println("Bienvenidos");
System.out.println("Estudio de la Division Politica de Guatemala");
System.out.println("Atencion!! para responder a las siguientes preguantas no utilice las letras mayusculas!!");
System.out.println("Creado por: WonderStudy");
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[0] +"?");
resp[0]= bf.readLine();
if((resp[0].equalsIgnoreCase(cab[0]))){
resp1[0]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[0]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[0]+":"+cab[0]);
} 
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[1] +"?");
resp[1]= bf.readLine();
if((resp[1].equalsIgnoreCase(cab[1]))){
resp1[1]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[1]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[1]+":"+cab[1]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[2] +"?");
resp[2]= bf.readLine();
if((resp[2].equalsIgnoreCase(cab[2]))){
resp1[2]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[2]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[2]+":"+cab[2]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[3] +"?");
resp[3]= bf.readLine();
if((resp[3].equalsIgnoreCase(cab[3])) ||(resp[3].equals(cab[3].toLowerCase()))){
resp1[3]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[3]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[3]+":"+cab[3]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[4] +"?");
resp[4]= bf.readLine();
if((resp[4].equalsIgnoreCase(cab[4]))){
resp1[4]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[4]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[4]+":"+cab[4]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[5] +"?");
resp[5]= bf.readLine();
if((resp[5].equalsIgnoreCase(cab[5]))){
resp1[5]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[5]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[5]+":"+cab[5]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[6] +"?");
resp[6]= bf.readLine();
if((resp[6].equalsIgnoreCase(cab[6].toUpperCase())) ||(resp[6].equals(cab[6].toLowerCase()))){
resp1[6]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[6]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[6]+":"+cab[6]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[7] +"?");
resp[7]= bf.readLine();
if((resp[7].equalsIgnoreCase(cab[7].toUpperCase())) ||(resp[7].equals(cab[7].toLowerCase()))){
resp1[7]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[7]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[7]+":"+cab[7]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[8] +"?");
resp[8]= bf.readLine();
if((resp[8].equalsIgnoreCase(cab[8])) ||(resp[8].equals(cab[8].toLowerCase()))){
resp1[8]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[8]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[8]+":"+cab[8]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[9] +"?");
resp[9]= bf.readLine();
if((resp[9].equalsIgnoreCase(cab[9])) ||(resp[9].equals(cab[9].toLowerCase()))){
resp1[9]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[9]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[9]+":"+cab[9]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[10] +"?");
resp[10]= bf.readLine();
if((resp[10].equalsIgnoreCase(cab[10])) ||(resp[10].equals(cab[10].toLowerCase()))){
resp1[10]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[10]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[10]+":"+cab[10]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[11] +"?");
resp[11]= bf.readLine();
if((resp[11].equalsIgnoreCase(cab[11])) ||(resp[11].equals(cab[11].toLowerCase()))){
resp1[11]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[11]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[11]+":"+cab[11]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[12] +"?");
resp[12]= bf.readLine();
if((resp[12].equalsIgnoreCase(cab[12])) ||(resp[12].equals(cab[12].toLowerCase()))){
resp1[12]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[12]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[12]+":"+cab[12]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[13] +"?");
resp[13]= bf.readLine();
if((resp[13].equalsIgnoreCase(cab[13])) ||(resp[13].equals(cab[13].toLowerCase()))){
resp1[13]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[13]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[13]+":"+cab[13]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[14] +"?");
resp[14]= bf.readLine();
if((resp[14].equalsIgnoreCase(cab[14])) ||(resp[14].equals(cab[14].toLowerCase()))){
resp1[14]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[14]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[14]+":"+cab[14]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[15] +"?");
resp[15]= bf.readLine();
if((resp[15].equalsIgnoreCase(cab[15])) ||(resp[15].equals(cab[15].toLowerCase()))){
resp1[15]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[15]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[15]+":"+cab[15]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[16] +"?");
resp[16]= bf.readLine();
if((resp[16].equalsIgnoreCase(cab[16])) ||(resp[16].equals(cab[16].toLowerCase()))){
resp1[16]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[16]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[16]+":"+cab[16]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[17] +"?");
resp[17]= bf.readLine();
if((resp[17].equalsIgnoreCase(cab[17])) ||(resp[17].equals(cab[17].toLowerCase()))){
resp1[17]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[17]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[17]+":"+cab[17]);
} 
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[18] +"?");
resp[18]= bf.readLine();
if((resp[18].equalsIgnoreCase(cab[18])) ||(resp[18].equals(cab[18].toLowerCase()))){
resp1[18]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[18]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[18]+":"+cab[18]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[19] +"?");
resp[19]= bf.readLine();
if((resp[19].equalsIgnoreCase(cab[19].toUpperCase())) ||(resp[19].equals(cab[19].toLowerCase()))){
resp1[19]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[19]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[19]+":"+cab[19]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[20] +"?");
resp[20]= bf.readLine();
if((resp[20].equalsIgnoreCase(cab[20].toUpperCase())) ||(resp[20].equals(cab[20].toLowerCase()))){
resp1[20]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[20]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[20]+":"+cab[20]);
}
System.out.println("");  
System.out.println("¿Cual es la Cabecera de:"+dep[21] +"?");
resp[21]= bf.readLine();
if((resp[21].equalsIgnoreCase(cab[21].toUpperCase())) ||(resp[21].equals(cab[21].toLowerCase()))){
resp1[21]=1;
System.out.println("La Respuesta es: Verdadera");
}else {
resp1[21]=0;   
System.out.println(" La Respuesta es: Falso, La Cabecera Departamental de "+dep[21]+":"+cab[21]);
}
int resp2;
resp2= resp1[0]+resp1[1]+resp1[2]+resp1[3]+resp1[4]+resp1[5]+resp1[6]+resp1[7]+resp1[8]
            +resp1[9]+resp1[10]+resp1[11]+resp1[12]+resp1[13]+resp1[14]+resp1[15]+resp1[16]+resp1[17]+resp1[18]+resp1[19]
            +resp1[20]+resp1[21];

System.out.println("Total:"+resp2+" de : 22");
    
}
   
}
